#include <stdlib.h>

#include "rotator.h"

struct image rotate(struct image const *input_image) {
    struct image output_image;
    output_image.width = input_image -> height;
    output_image.height = input_image -> width;
    struct pixel *data;
    data = malloc(input_image -> width * input_image -> height * sizeof(struct pixel));
    for (size_t i = 0; i < input_image -> height; i = i + 1) {
        for (size_t j = 0; j < input_image -> width; j = j + 1) {
            data[j * input_image -> height + (input_image -> height - 1 - i)] = input_image -> data[i * input_image -> width + j];
        }
    }
    output_image.data = data;
    return output_image;
}
