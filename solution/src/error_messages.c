#include "error_messages.h"

const char *read_messages[] = {
        [READ_NO_DATA] = "No data found",
        [READ_ERROR] = "Couldn't open this file"
};

const char *write_messages[] = {
        [WRITE_ERROR] = "Couldn't open file to write",
        [WRITE_OK] = "Successfully rotated"
};

const char *open_message(enum read_status status) {
    return read_messages[status];
}

const char *close_message(enum write_status status) {
    return write_messages[status];
}
