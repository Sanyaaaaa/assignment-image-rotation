#include <stdint.h>

#include "bmp_header.h"
#include "image.h"

struct bmp_header new_bmp_header(const struct image *image) {
    struct bmp_header header = {
            .bfType =  0x4D42,
            .bfileSize = sizeof(struct bmp_header) + padding(image) *  image -> height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image -> width,
            .biHeight = image -> height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image -> height * (image -> width + padding(image)),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

uint8_t padding(struct image const *image) {
    return image -> width % 4 == 0 ? 0 : 4 - ((image -> width * sizeof(struct pixel)) % 4);
}
