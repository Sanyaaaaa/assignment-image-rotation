#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "bmp.h"
#include "bmp_header.h"

static void write_data(FILE *out, struct image const *image) {
    const uint8_t image_padding = padding(image);
    const uint32_t extra = 0;
    for (size_t i = 0; i < image -> height; i = i + 1) {
        fwrite(&image -> data[i * image -> width], sizeof(struct pixel), image -> width, out);
        fwrite(&extra, 1, image_padding, out);
    }
}

enum read_status read_from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, in);
    image->width = header.biWidth;
    image->height = header.biHeight;
    struct pixel *data = malloc(image->width * image->height * sizeof(struct pixel));
    const uint8_t image_padding = padding(image);
    for (size_t i = 0; i < image->height; i = i + 1) {
        if(!fread(&(data[i * image->width]), sizeof(struct pixel), image->width, in)) {
            return READ_ERROR;
        }
        fseek(in, image_padding, SEEK_CUR);
    }
    image->data = data;
    return READ_OK;
}

enum read_status open_file(char* input_file, struct image *image) {
    FILE *in = fopen(input_file, "rb");
    if (!in) {return READ_NO_DATA;}
    enum read_status status = read_from_bmp(in, image);
    return status;
}

enum write_status write_to_bmp(FILE *out, struct image const *image) {
    struct bmp_header header = new_bmp_header(image);
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    write_data(out, image);
    return WRITE_OK;
}

enum write_status save_file(char* output_file, struct image const *image) {
    FILE *out = fopen(output_file, "wb");
    if (!out) {return WRITE_ERROR;}
    enum write_status status = write_to_bmp(out, image);
    return status;
}
