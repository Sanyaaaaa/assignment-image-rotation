#include <stdlib.h>

#include "bmp.h"
#include "error_messages.h"
#include "rotator.h"

int main(int argc, char**argv) {
    if (argc < 2) {
        fprintf(stderr, "Not enough arguments");
    }
    if (argc >2) {
        fprintf(stderr, "Too many arguments");
    }
    struct image image = {0};
    enum read_status open_status = open_file(argv[1], &image);
    if (open_status != READ_OK) {
        fprintf(stderr, "ERROR: %s\n", open_message(open_status));
        return 1;
    }
    struct image result = rotate(&image);
    enum write_status close_status = save_file(argv[2], &result);
    if (close_status != WRITE_OK) {
        fprintf(stderr, "ERROR: %s\n", close_message(close_status));
        return 2;
    }
    else {
        fprintf(stderr, "%s\n", close_message(close_status));
    }
    free(image.data);
    free(result.data);
    return 0;
}
