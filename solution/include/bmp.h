#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "bmp_header.h"

enum read_status {
    READ_OK = 0,
    READ_NO_DATA,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status open_file(char* input_file, struct image *image);

enum write_status save_file(char* output_file, struct image const *image);

#endif
