#ifndef ROTATOR_H
#define ROTATOR_H

#include "image.h"

struct image rotate(struct image const *input_image);

#endif
