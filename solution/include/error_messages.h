#ifndef ERROR_MESSAGES_H
#define ERROR_MESSAGES_H

#include "bmp.h"

const char *open_message(enum read_status status);

const char *close_message(enum write_status status);

#endif
